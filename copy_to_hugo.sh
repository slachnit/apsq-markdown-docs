#!/bin/bash

CONVCMD="python3 $HOME/Projects/apsq-docs/apsq-markdown-docs/convert_markdown.py hugo"
INDIR=$HOME/Projects/apsq-docs/apsq-markdown-docs/markdown
TMPDIR=$XDG_RUNTIME_DIR/apsq
OUTDIR=$HOME/Projects/apsq-docs/apsq-docs-hugo/content/docs

cd $INDIR
rm -rf $TMPDIR
mkdir $TMPDIR

for file in $(find . -name '*.md'); do
  mkdir -p $TMPDIR/$(dirname $file)
  $CONVCMD $file $TMPDIR/$file
done

for file in $(find . -name '*.png'); do
  mkdir -p $TMPDIR/$(dirname $file)
  cp $file $TMPDIR/$file
done

rsync -avz --quiet --delete $TMPDIR/* $OUTDIR
rm -rf $TMPDIR
