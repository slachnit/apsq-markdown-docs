---
# SPDX-FileCopyrightText: 2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Acknowledgments"
---

Allpix Squared is developed and maintained by

* Paul Schütze, DESY, @pschutze
* Simon Spannagel, DESY, @simonspa
* Koen Wolters, @kwolters

The following authors, in alphabetical order, have developed or contributed to Allpix Squared:

* Mohamed Moanis Ali, GSOC2019 Student, @mmoanis
* Mathieu Benoit, BNL, @mbenoit
* Thomas Billoud, Université de Montréal, @tbilloud
* Tobias Bisanz, CERN, @tbisanz
* Marco Bomben, Université de Paris, @mbomben
* Koen van den Brandt, Nikhef, @kvandenb
* Carsten Daniel Burgard, DESY, @cburgard
* Maximilian Felix Caspar, DESY, @mcaspar
* Liejian Chen, Institute of High Energy Physics Beijing, @chenlj
* Manuel Alejandro Del Rio Viera, DESY, @mdelriov
* Katharina Dort, University of Gie\ss en, @kdort
* Neal Gauvin, Université de Genève, @ngauvin
* Lennart Huth, DESY, @lhuth
* Daniel Hynds, University of Oxford, @dhynds
* Maoqiang Jing, University of South China, Institute of High Energy Physics Beijing, @mjing
* Moritz Kiehn, Université de Genève, @msmk
* Rafaella Eleni Kotitsa, CERN, @rkotitsa
* Stephan Lachnit, DESY, @slachnit
* Salman Maqbool, CERN Summer Student, @smaqbool
* Stefano Mersi, CERN, @mersi
* Ryuji Moriya, CERN Summer Student, University of Glasgow, @rmoriya
* Sebastien Murphy, ETHZ, @smurphy
* Andreas Matthias Nürnberg, KIT, @nurnberg
* Sebastian Pape, TU Dortmund University, @spape
* Marko Petric, CERN, @mpetric
* Radek Privara, Palacky University Olomouc, @rprivara
* Nashad Rahman, The Ohio State University, @nashadroid
* Sabita Rao, GSDocs2020 Student, @srao
* Edoardo Rossi, DESY, @edrossi
* Andre Sailer, CERN, @sailer
* Enrico Jr. Schioppa, Unisalento and INFN Lecce, @schioppa
* Sebastian Schmidt, FAU Erlangen, @schmidtseb
* Sanchit Sharma, Kansas State University, @SanchitKratos
* Xin Shi, Institute of High Energy Physics Beijing, @xshi
* Petr Smolyanskiy, Czech Technical Univbersity Prague, @psmolyan
* Viktor Sonesten, GSOC2018 Student, @tmplt
* Reem Taibah, Université de Paris, @retaibah
* Ondrej Theiner, Charles University, @otheiner
* Annika Vauth, University of Hamburg, @avauth
* Mateus Vicente Barreto Pinto, CERN, @mvicente
* Håkan Wennlöf, DESY, @hwennlof
* Andy Wharton, Lancaster University, @awharton
* Morag Williams, University of Glasgow, @williamm

The authors would also like to express their thanks to the developers of AllPix \[[@ap1wiki], [@ap1git]\].


[@ap1wiki]: https://twiki.cern.ch/twiki/bin/view/Main/AllPix
[@ap1git]: https://github.com/ALLPix/allpix
