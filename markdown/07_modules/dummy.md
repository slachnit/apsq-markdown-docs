---
# SPDX-FileCopyrightText: 2017-2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0 OR MIT
title: "Dummy"
description: "A dummy module"
module_maintainer: "*NAME* (*EMAIL*)"
module_status: "Functional"
module_input: "*INPUT_MESSAGE if applicable*"
module_output: "*OUTPUT_MESSAGE if applicable*"
---

## Description
*Short description of this module*

## Parameters
* `param`: *explanation with optional default*

## Usage
*Example how to use this module*
