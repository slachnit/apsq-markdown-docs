---
# SPDX-FileCopyrightText: 2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Modules"
description: "Description of all available Allpix Squared modules."
weight: 7
---

This section describes all currently available modules in detail. This includes a description of the physics implemented as
well as possible configuration parameters along with their defaults. For inquiries about certain modules or its
documentation, the respective maintainers should be contacted directly. The modules are listed in alphabetical order.
