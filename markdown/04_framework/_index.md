---
# SPDX-FileCopyrightText: 2022 CERN and the Allpix Squared authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Framework"
description: "The structure & components of the framework."
weight: 4
---

This chapter details the technical implementation of the Allpix Squared framework and is mostly intended to provide insight
into the gearbox to potential developers and interested users.
