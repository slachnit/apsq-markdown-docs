---
# GLFM and hugo support front matter written in YAML
title: "This is the title for the corresponding webpage"
---

GLFM supports LaTeX inline math like $`\frac{1}{2}`$. GLFM also
supports free standing equations:

```math
\mu_e^{-1}(E) = 1 / \mu_{0,e} + E / v_{sat}
```
