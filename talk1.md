# This is a large caption in Markdown

## This is also a caption but a bit smaller

Text in Markdown can be *italics* or **bold**. 
Markdown supports [hyperlinks](https://cern.ch/allpix-squared/).

Markdown support code highlighting `inline` and via blocks:

```cpp
auto hist = CreateHistogram<TH1D>("name", "title", 100, 0., 100.);
```
