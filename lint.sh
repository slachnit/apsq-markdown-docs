#!/usr/bin/sh

flake8 convert_markdown.py --max-line-length=125
pylint convert_markdown.py --max-line-length=125
