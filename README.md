# APSQ Markdown docs

APSQ documentation in GitLab Markdown for use with hugo & pandoc

Idea wrt relative links:

- Convert GLFM to pandoc for all files, then put all in one giant file, fix links in there and

Ideas wrt pandoc filters:

- Minted: https://github.com/pandoc/lua-filters/tree/master/minted

- Fix Links: https://stackoverflow.com/questions/48569597/pandoc-filters-change-relative-paths-to-absolute-paths

- Fix Links: https://gist.github.com/MyriaCore/75729707404cba1c0de89cc03b7a6adf#file-fix-links-lua

- GitLab Math: https://gist.github.com/lierdakil/00d8143465a488e0b854a3b4bf355cf6#file-gitlab-math-lua

- Front matter to title
